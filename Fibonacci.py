class Fibonacci:
    def __init__(self):
        self.iteracao = int(input("Digite a quantidade de elementos: "))
        self.anterior = 0
        self.proximo = 1
        self.valor_atual = 0
        self.lista_elementos = []
        if (self.iteracao == 0):
            pass
        else:
            while self.iteracao != 0:
                self.lista_elementos.append(self.anterior)
                self.valor_atual = self.anterior + self.proximo
                self.anterior = self.proximo
                self.proximo = self.valor_atual
                self.iteracao -= 1
        lista_fibo = { chave+1 : valor for chave, valor in enumerate(self.lista_elementos) }
        print(lista_fibo)

sequencia = Fibonacci()
